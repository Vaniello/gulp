module.exports = function() {
    $.gulp.task("images", function() {
        return $.gulp
          .src([
            "./src/static/img/**/*.{jpg,jpeg,png,gif}",
            "!./src/static/img/svg/icons/*",
            "!./src/static/img/favicons/*.{jpg,jpeg,png,gif}"
          ])
          .pipe($.newer("./build/img/"))
          .pipe($.imagemin([
              $.imagemin.gifsicle({ interlaced: true }),
              $.imagemin.jpegtran({ progressive: true }),
              $.imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
              }),
              $.imagemin.svgo({ plugins: [{ removeViewBox: true }] }),
              $.imagemin.optipng({ optimizationLevel: 5 }),
              $.pngquant({ quality: "65-70", speed: 5 })
            ]))
          .pipe($.gulp.dest("./build/img/"))
          .pipe($.debug({ title: "images" }))
          .on("end", $.browsersync.reload);
    });
};