module.exports = function() {
    $.gulp.task("watch", function() {
        return new Promise((res, rej) => {
            $.watch(["./src/blocks/**/*.pug"], $.gulp.series("pug"));
            $.watch("./src/styles/**/*.styl", $.gulp.series("styles"));
            $.watch(["./src/static/img/**/*.{jpg,jpeg,png,gif}", "!./src/static/img/icons/svg/*.svg", "!./src/static/img/favicons/*.{jpg,jpeg,png,gif}"], $.gulp.series("images"));
            $.watch("./src/js/**/*.js", $.gulp.series("scripts"));
            res();
        });
    });
};