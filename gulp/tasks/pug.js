module.exports = function() {
    $.gulp.task("pug", function() {
        return $.gulp.src(["./src/blocks/index.pug"])
            .pipe($.pug({pretty: true}))
            .pipe($.replace("../build/", "../"))
            .pipe($.gulp.dest("./build/"))
            .pipe($.debug({"title": "html"}))
            .on("end", $.browsersync.reload);
    });
};